# SpringCloudDemo

#### 介绍
 这是一个SpringCloud Demo，主要包括Eureka服务注册与发现、Ribbon负载均衡、 Feign负载均衡、Hystrix断路器、zuul路由网关、SpringCloud Config分布式配置中心

内容是根据尚硅谷的SpringCloud学习，并编写的。

SpringCloud笔记.md 是基于 https://github.com/gongxings/spring-cloud-study  补充改写的。
