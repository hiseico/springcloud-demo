package cn.sitcat.springcloud.service;

import cn.sitcat.springcloud.entities.Dept;

import java.util.List;

/**
 * @author hiseico
 * @date 2019/5/6 15:10
 * @desc
 */
public interface DeptService {
    public boolean add(Dept dept);

    public Dept get(Long id);

    public List<Dept> list();
}
