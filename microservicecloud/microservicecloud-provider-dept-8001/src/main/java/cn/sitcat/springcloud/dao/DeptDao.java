package cn.sitcat.springcloud.dao;

import cn.sitcat.springcloud.entities.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author hiseico
 * @date 2019/5/6 14:49
 * @desc
 */
@Mapper
public interface DeptDao {
    public boolean addDept(Dept dept);

    public Dept findById(Long id);

    public List<Dept> findAll();
}
