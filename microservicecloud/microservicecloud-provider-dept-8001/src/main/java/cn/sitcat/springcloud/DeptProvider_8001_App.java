package cn.sitcat.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author hiseico
 * @date 2019/5/6 15:28
 * @desc
 */
@SpringBootApplication
@EnableEurekaClient //本服务启动后自动注册进eureka服务中
@EnableDiscoveryClient //
public class DeptProvider_8001_App {
    public static void main(String[] args) {
        SpringApplication.run(DeptProvider_8001_App.class, args);
    }
}
