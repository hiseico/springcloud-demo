package cn.sitcat.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author hiseico
 * @date 2019/5/5 22:23
 * @desc
 */
@SuppressWarnings("serial")
@AllArgsConstructor //使用lombok生成全参构造函数
@NoArgsConstructor //使用lombok生成无参构造函数
@Data //为每一个参数设置get、set
@Accessors(chain = true) //链式风格访问
public class Dept implements Serializable {
    /**
     * 主键
     */
    private Long deptno;

    /**
     * 部门名称
     */
    private String dname;

    /**
     * 来自哪个数据库，因为微服务架构可以一个服务对应一个数据库，同一个信息被存储到不同数据库
     */
    private String db_source;


    public static void main(String[] args) {
        Dept dept = new Dept();
        dept.setDeptno(1L).setDname("hahah").setDb_source("mysql");//链式风格写法
    }
}


