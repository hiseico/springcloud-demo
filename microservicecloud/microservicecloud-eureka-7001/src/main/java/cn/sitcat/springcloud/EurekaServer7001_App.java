package cn.sitcat.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author hiseico
 * @date 2019/5/6 22:41
 * @desc EurekaServer服务端启动类，接收其他微服务注册进来
 */
@SpringBootApplication
@EnableEurekaServer //启用eureka服务端注解
public class EurekaServer7001_App {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer7001_App.class, args);
    }
}
